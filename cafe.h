#include "Organization.h"
#include <vector>
#include "Drinks.h"
#include "Foods.h"
#include "Menu.h"
#include <fstream>
using std::ifstream;
using std::ofstream;
//Реализуем класс Кафе, который наследуется от Организации
template<class T,class Person> //Столы либо номируем, либо называем
class Cafe:public OrganizationCate
{
private:
	Menu<Drinks, string>* drs;//Напитки
	Menu<Foods, string>* fods;//Еда
	vector<T>stol;//Название стола
	double usluga;//Цена за услугу
	bool typeUslug;//Самообслуживания-1 или нет-0
	Person curVlad;//ФИО и контакты владельца
	vector<Person>sotrudniki;//В далнейшей можно реализовать отдельный класс
public:
	Cafe(string name, vector<T>& st,bool typeUslug=1);//Название конструктора
	~Cafe();//Деструктор
	void setUsluga(double pr = 0);//По умолчание 0р
	double getOrder();//Заказ
	void setSamoObsluj(bool bl = false);
	string Information() override;//Информация

};
//Шаблонный класс реализуем тут все
template<class T, class Person>
inline Cafe<T, Person>::Cafe(string name, vector<T>& st, bool typeUslug):OrganizationCate(name)
{
	this->stol.assign(st.begin(), st.end());//Установка стола
	this->typeOrgan = OrganizationCate::Cafe;//Кафе
	this->typeUslug = 0;//Не самообслуживание
	vector<Drinks>dv;
	ifstream inp("Drinks.txt");
	while (!inp.eof())
	{
		Drinks dr;
		inp >> dr;
		dv.push_back(dr);
		//cout << dr << endl;

	}
	inp.close();
	vector<Foods>fd;
	ifstream inp2("Foods.txt");
	while (!inp2.eof())
	{
		Foods dr;
		inp2 >> dr;
		fd.push_back(dr);
	//	cout << dr << endl;

	}
	inp2.close();
	this->drs = new Menu<Drinks, string>(dv, "++++++++++++++++++++++++++++++++++++++++++++++++++++++");
	this->fods = new Menu<Foods, string>(fd, "++++++++++++++++++++++++++++++++++++++++++++++++++++++");

}

template<class T, class Person>
inline Cafe<T, Person>::~Cafe()
{
	//очистка 
	delete drs;
	delete fods;
}

template<class T, class Person>
inline void Cafe<T, Person>::setUsluga(double pr)
{
	this->usluga = pr;//Цена за услуги
}

template<class T, class Person>
inline double Cafe<T, Person>::getOrder()
{
	double ans = 0.0;
	bool quit = false;
	while (!quit)
	{
		cout << "1 -Drinks\n";
		cout << "2 -Foods\n";
		cout << "3-Exit\n";
		int cmd;
		cin >> cmd;
		switch (cmd)
		{
		case 1:
		{
			ans += drs->GetChoose();

			break;
		}
		case 2:
		{
			ans += fods->GetChoose();
			break;
		}
		case 3:
		{
			quit = true;
			break;
		}
		default:
			break;
		}
	}
	if (typeUslug == 0)//Если не самообслуживание
	{
		ans += usluga;
	}
	cout << "С вас " << ans << " руб" << endl;
	return ans;
}

template<class T, class Person>
inline void Cafe<T, Person>::setSamoObsluj(bool bl)
{
	this->typeOrgan = bl;
}

template<class T, class Person>
inline string Cafe<T, Person>::Information()
{
	//Тип + имя владелца
	return this->getName() + "|" + getType() + "|" + this->curVlad ;
}
