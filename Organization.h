#include <string>
using std::string;
//Реализуем  класс Организация
class OrganizationCate
{
protected:
	string name;    //Название 
public:
	enum Type   //Тип организации
	{
		Bar,         //Бар
		Cafe,       //Кафе
		Fast_Food,  //Фастфуды
		Restaurant, //Ресторан
		DinningRoom //Столовая
	};
protected:
	Type typeOrgan; //Тип Организации
	bool isWifi;    //Наличие Wifi
	//Реализуем переобразование Type в Str
	string TypeToStr(OrganizationCate::Type t)const;
	//Реализуем переобразование строку в TypeOrg
	Type StrToType(const string &s)const;
public:
	OrganizationCate(); //Конструктор по умолчанию
	OrganizationCate(const string& _name);  //Конструктор с параметрами названия организации
	OrganizationCate(const OrganizationCate& org);  //Конструктор копирования
	void setName(const string& n);//Установка название
	void setType(OrganizationCate::Type tp = Restaurant);   //Установка типа организации, по умолчанию Ресторан
	void setWifiEnabled(bool bl = false);   //Установка Wifi
	string getName()const;
	string getType()const;  //Вернет тип Орг
	bool getWifi()const;    //наличие Wifi
	virtual string Information();   //Информация об организация для каждого наследника своя
	~OrganizationCate()
	{
		
	}
};
