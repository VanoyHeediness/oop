#include <iostream>
#include <string>
#include <vector>
#include "Drinks.h"
#include "Foods.h"

//Реализуем класс Меню
template<class T,class U>	//Асортимент продукции
//T-тип продукции
//U загаловок меню 
class Menu
{
private:
	vector<T>prod;	//Продукция
	U header;	//Заголовок   горизонтальный
public:
	Menu(vector<T>& in,const U head);	//Конструктор, параметр которого собственный класс продукции
	double GetChoose();	//Выбор клиента
	~Menu()
	{

	}

};

template<class T, class U>
Menu<T, U>::Menu(vector<T>& in, const U head)
{
	this->prod.assign(in.begin(), in.end());
	this->header = head;
}

template<class T, class U>
//Возврашаем общую стоимость
double Menu<T, U>::GetChoose()
{
	bool quit = false;
	while (!quit)
	{
		int i = 0;
		cout << header << endl;
		for (i = 0; i < prod.size(); i++)
		{
			cout << i + 1 << "-->>" << prod[i] << endl;
		}
		cout << i + 1 << "--->>Хватит\n";
		cout << "Выбирайте по индексам\n";
		int cmd;
		cin >> cmd;
		if (cmd == i + 1)
			quit = true;
		else
		{
			prod[cmd - 1]++;	//Заказано одно
			cout << prod[cmd - 1].getCnt() << endl;
		}
	}
	double allcost = 0;
	//Возвращаем, где количество заказов больше нуля
	for (int i = 0; i < prod.size(); i++)
	{
		if (prod[i].getCnt() > 0)
			allcost += prod[i].getCnt() * prod[i].getCost();
	}
	return allcost;	//Цена без услуги
}
