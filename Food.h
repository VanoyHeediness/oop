#include <string>
#include <vector>
#include <stdlib.h>
#include <iostream>
#include <iomanip>
#include "Function.h"
using namespace std;

class Foods
{
private:
	string name;	//Название 
	string time;	//Время приготовление
	double cost;	//Стоимость 
	string type;	//Ограничения
	int cnt;		//Количество 
public:
	Foods();
	Foods(const string& _name, const string& time, const double _c, string tp);//Конструктор с параметрами
	Foods(const Foods& dr);	//Конструктор копирования, кроме количества
	void setName(const string& s);	//Установка названия
	void setType(const string& t);
	void setCost(double c);
	void setCnt(const int cn);
	void setTime(const string& s);//Установка время приготовления
	
	string getName()const;
	string getType()const;
	double getCost()const;
	int getCnt()const;
	string getTime()const;
	//Переопрелим некторое операции
	Foods& operator=(const Foods& dr);//перегрузка сравнивания
	bool operator==(const Foods& dr);//Проверка на равность нужна для поиска в Кафе
	//Перегрузим инкремент и декремент для увеличения или уменьшения количества
	Foods& operator--(int);//-- postfix версия
	Foods& operator++(int);//++
	//Перегружим вывод и вывод
	friend istream& operator>>(istream& in, Foods& dr);
	friend ostream& operator<<(ostream& out, const Foods& dr);
	~Foods()
	{

	}
};
