#include <string>
#include <vector>
#include <stdlib.h>
#include <iostream>
#include <iomanip>
#include "Function.h"
using namespace std;

class Drinks
{
private:
	string name;	//Название 
	string type;	//тип напитка
	double cost;	//Стоимость 
	bool isLitse;	//наличие лицензия
	int cnt;		//Количество 
public:
	Drinks();
	Drinks(const string& _name, const string& _type, const double _c, bool is);	//Конструктор с параметрами
	Drinks(const Drinks& dr);		//Конструктор копирования, кроме количество обекта
	void setName(const string& s);	//Установка название
	void setType(const string& t);	//Установка типа
	void setCost(double c);			//Установка стоимости
	void setLitsensiya(bool is);
	void setCnt(const int cn);		//Установка количества
	
	string getName()const;
	string getType()const;
	double getCost()const;
	bool getLitsen()const;
	int getCnt()const;
	//Переопределим некторое операции
	Drinks& operator=(const Drinks& dr);	//перегрузка сравнивания
	bool operator==(const Drinks& dr);		//Проверка на равность нужна для поиска в Кафе
	//Перегрузим инкремент и декремент для увеличения и уменьшения количества напитков
	Drinks& operator--(int);//-- postfix версия
	Drinks& operator++(int);//++
	//Перегружим вывод и вывод
	friend istream& operator>>(istream& in, Drinks& dr);
	friend ostream& operator<<(ostream& out, const Drinks& dr);
};

